package com.koval.View;

import com.koval.annotation.CustomAnnotation;
import com.koval.model.Model;
import org.apache.logging.log4j.*;
import com.koval.receiver.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.stream.Collectors;

public class View {

    private static Logger logger = LogManager.getLogger(View.class);
    private Scanner scanner = new Scanner(System.in);
    private Map<String,String>options;
    private Map<String,Executable>methods;

    public View() {
        options = new LinkedHashMap<>();
        options.put("1", "1 - Task #1");
        options.put("2", "2 - Task #2");
        options.put("3", "3 - Task #3");
        options.put("4", "4 - Task #4");
        options.put("5", "5 - Task #5");
        options.put("6", "6 - Task #6");
        options.put("Q", "Q - Exit");

        methods = new LinkedHashMap<>();
        methods.put("1",this::task1);
        methods.put("2",this::task2);
        methods.put("3",this::task3);
        methods.put("4",this::task4);
        methods.put("5",this::task5);
        methods.put("6",this::task6);
        methods.put("Q", this::exit);
    }

    private void task1(){
        Class<? extends Model> cls = Model.class;
        Field[] f = cls.getDeclaredFields();
        List<String> list = Arrays.stream(f)
                .map(s -> Arrays.toString(new String[]{s.getName(), s.getType().getSimpleName(),
                        Modifier.toString(s.getModifiers())}))
                .collect(Collectors.toList());
        logger.trace(list);
    }

    private void task2(){
        Class<? extends Model> cls = Model.class;
        Field[]fields = cls.getDeclaredFields();
        for (Field field:fields) {
            if(field.isAnnotationPresent(CustomAnnotation.class)) {
                logger.trace("\nField: " + field.getName() + "\n");
                logger.trace("Annotation info: ");
                CustomAnnotation annotation = field.getDeclaredAnnotation(CustomAnnotation.class);
                logger.trace( annotation.value()+ " " + annotation.otherValue());
            }
        }
    }

    private void task3(){
        logger.trace("Invocation of 3 methods\n");
        Class<? extends Model> cls = Model.class;
        try {
            Method first = cls.getDeclaredMethod("getSomeInteger");
            first.setAccessible(true);
            logger.trace("First method returns : ");
            logger.trace(first.invoke(new Model()));
            logger.trace("\n");

            Model model = new Model();
            logger.trace("Field before invocation: " + model.getStringField() + "\n");
            Method second = cls.getDeclaredMethod("setStringField", String.class);
            second.setAccessible(true);
            second.invoke(model,"Invocation string");
            logger.trace("Field after invocation: " + model.getStringField() + "\n");

            model.setSomeDouble(2.5);
            logger.trace("Field before invocation: " + model.getSomeDouble() + "\n");
            Method third = cls.getDeclaredMethod("setSomeDouble", double.class);
            third.invoke(model,3.0);
            logger.trace("Field after invocation: " + model.getSomeDouble() + "\n");


        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void task4() throws IllegalAccessException {
        Class<? extends Model> cls = Model.class;
        Model model = new Model();
        Field[]fields = cls.getDeclaredFields();
        Random rd = new Random();
        Field random = fields[rd.nextInt(fields.length)];
        random.setAccessible(true);
        logger.trace("Random field name: " + random.getName() + "\n");
        logger.trace("Random field value before setting: " + random.get(model) + "\n");

        if(random.getType() == double.class){
            random.setDouble(model,7.7);
        }else if(random.getType() == int.class){
            random.setInt(model,77);
        }else {
            random.set(model,"Random string");
        }

        logger.trace("Random field value after setting: " + random.get(model) + "\n");
    }

    private void task5() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Class<? extends Model> cls = Model.class;
        Model model = new Model();

        logger.trace("Invocation of String[] myMethod\n");
        Method first = cls.getDeclaredMethod("myMethod", String[].class);
        String[]args = {"one", "two", "three", "four", "five"};
        first.invoke(model, (Object) args);

        logger.trace("\nInvocation of String,int[] myMethod\n");
        Method second = cls.getDeclaredMethod("myMethod", String.class, int[].class);
        second.setAccessible(true);
        second.invoke(model,"Integers",new int[]{1,2,3,4,5});
    }

    private void task6(){
        new Receiver(new Model());
    }

    private void show(){
        logger.trace("\n~~~~~~~~~~MENU~~~~~~~~~~\n");
        for (String option:options.values()) {
            logger.trace(option + "\n");
        }
        logger.trace("~~~~~~~~~~MENU~~~~~~~~~~\n\n");
    }

    private void exit(){
        logger.trace("Bye!");
        System.exit(0);
    }

    public void interact()  {
        String key;

        do {
            show();
            logger.trace("--->");
            key = scanner.nextLine().toUpperCase();

            if(!methods.containsKey(key)){
                logger.trace("Wrong input, try again");
            }
            else {

                try {
                    methods.get(key).execute();
                } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }

        }while (!key.equals("Q"));

    }
}
