package com.koval.View;

import java.lang.reflect.InvocationTargetException;

@FunctionalInterface
public interface Executable {
    void execute() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException;
}
