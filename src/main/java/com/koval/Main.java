package com.koval;

import com.koval.View.View;

import java.lang.reflect.InvocationTargetException;

public class Main {

    public static void main(String[] args) {

        try {
            new View().interact();
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

    }
}
