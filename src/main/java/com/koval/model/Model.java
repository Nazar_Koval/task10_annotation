package com.koval.model;

import com.koval.annotation.CustomAnnotation;
import org.apache.logging.log4j.*;
import java.util.Arrays;

public class Model {

    private static Logger logger = LogManager.getLogger(Model.class);
    @CustomAnnotation(value = "Captain", otherValue = "Price")
    private String stringField;
    @CustomAnnotation("Captain")
    private int someInteger;
    private double someDouble;

    public void setSomeDouble(final double someDouble){
        this.someDouble = someDouble;
    }

    private void setSomeInteger(final int someInteger){
        this.someInteger = someInteger;
    }

    private void setStringField(final String stringField){
        this.stringField = stringField;
    }

    public double getSomeDouble() {
        return someDouble;
    }

    private int getSomeInteger(){
        return someInteger;
    }

    public String getStringField() {
        return stringField;
    }

    @Override
    public String toString(){
        return "String field: " + this.stringField + "\n"
                + "Integer field: " + this.someInteger + "\n"
                + "Double field: " + this.someDouble + "\n";
    }

    private void myMethod(String str, int...args){
        logger.trace("First arg passed: " + str + "\n");
        logger.trace("Passed integer arguments:\n");
        logger.trace("|");
        Arrays.stream(args).forEach(s -> logger.trace(s + "|"));
    }

    public void myMethod(String...args){
        logger.trace("Passed string arguments: \n");
        logger.trace("|");
        Arrays.stream(args).forEach(s -> logger.trace(s + "|"));
    }
}
