package com.koval.receiver;

import org.apache.logging.log4j.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;

public class Receiver {

    private static Logger logger = LogManager.getLogger(Receiver.class);

    public Receiver(final Object object){
        allInfo(object);
    }

    private void allInfo(final Object object){
        Class<?> cls = object.getClass();
        logger.trace("|||||All the information about " + cls.getSimpleName() + " |||||\n");

        Field[]fields = cls.getDeclaredFields();
        logger.trace("\nList of declared fields: \n");
        logger.trace(Arrays.toString(fields));

        Method[]methods = cls.getDeclaredMethods();
        logger.trace("\n\nList of declared methods:\n");
        logger.trace(methods);

        logger.trace("\n\n~~~~~Specific information about fields~~~~~\n");

        for (Field field:fields) {
            logger.trace("Field name: " + field.getName() + "\n");
            logger.trace("Field type: " + field.getType().getSimpleName() + "\n");
            logger.trace("Field modifiers: " + Modifier.toString(field.getModifiers()) + "\n");
            Annotation[]fieldAnnotations = field.getAnnotations();

            if(fieldAnnotations.length == 0){
                logger.trace("No annotations for this field\n");
            }
            else {
                logger.trace("Annotations for this field: \n");

                for (Annotation a : fieldAnnotations) {
                    logger.trace(a.getClass().getSimpleName() + " ");
                }
                logger.trace("\n");
            }

            logger.trace("\n");
        }

        logger.trace("~~~~~Specific information about methods~~~~~\n");

        for (Method method:methods) {
            logger.trace("Method name: " + method.getName() + "\n");
            logger.trace("Method return type: " + method.getReturnType().getSimpleName() + "\n");
            logger.trace("Method modifiers: " + Modifier.toString(method.getModifiers()) + "\n");
        }
    }
}
